# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

Style-Guide-Boilerplate v3.3.0
==============================

A starting point for crafting living style guides.

[View Demo](http://brettjankord.com/projects/style-guide-boilerplate/)

*Note: Sample patterns have been included in the demo. Your site will have its own unique patterns.*

![Screenshot](http://brettjankord.com/projects/style-guide-boilerplate/style-guide-boilerplate-v3.0.0.jpg)

## Getting Started With Style Guide Boilerplate

### Download the Style Guide Boilerplate
You can clone, fork, or download the repo from GitHub.
Once you have the files for **Style Guide Boilerplate**, you'll create a directory on your site for them.

### Set up a directory on your site for the style guide
I recommend creating a directory named `style-guide` in your site's root directory. I think it would be awesome if I could go to `anysite.com/style-guide/` and check out that site's style guide.

### Upload the Style Guide Boilerplate files
**Style Guide Boilerplate** is currently PHP based so you will need a server that supports PHP. Just upload the files from the GitHub repo to your newly created directory and your almost done.

### Hook up your own CSS into the style guide
In the `<head>` of **Style Guide Boilerplate** are custom styles for the boilerplate itself. These have all been prefixed with sg- so they hopefully shouldn't cause any conflicts with your website's own styles.

Below the custom styles for the boilerplate, you will add in your own custom stylesheet(s) which you use on your live site.

    <!-- Style Guide Boilerplate Styles -->
    <link rel="stylesheet" href="css/sg-style.css">

    <!-- Replace below stylesheet with your own stylesheet -->
    <link rel="stylesheet" href="css/style.css">


### Review your live site CSS
You should be able to go to `yoursite.com/style-guide/` and see how your live site's CSS affects base elements.
The last step is creating your sites custom patterns/modules.


## Browser Support
Known supported browsers include:

* Chrome
* Firefox
* Safari
* Opera
* IE10+
* Safari for iOS
* Stock Android Browser (4.0+)

## Ressources complémentaires
[Styleguides.io](http://styleguides.io)

## Ports
* A nodejs port using handlebars is available at [Style-Guide-Boilerplate-nodejs](https://github.com/DeadlyBrad42/Style-Guide-Boilerplate-nodejs).
* A ruby port avaiable at [Rails_App_Style_Guide](https://github.com/JoshuaMSchultz/Rails_App_Style_Guide)

